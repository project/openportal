core = 8.x
api = 2

; Modules
projects[admin_toolbar][version] = "1.24"
projects[adminimal_admin_toolbar][version] = "1.8"
projects[checklistapi][version] = "1.8"
projects[ctools][version] = "3.0"
projects[eu_cookie_compliance][version] = "1.2"
projects[gdpr][version] = "2.0-alpha4"
projects[google_analytics][version] = "2.3"
projects[metatag][version] = "1.7"
projects[openportal_core][version] = "1.x-dev"
projects[pathauto][version] = "1.3"
projects[redirect][version] = "1.3"
projects[search404][version] = "1.0"
projects[social_media_links][version] = "2.6"
projects[token][version] = "1.5"
projects[xmlsitemap][version] = "1.0-alpha3"
projects[yoast_seo][version] = "1.4"

; Themes
projects[adminimal_theme][version] = "1.3"

